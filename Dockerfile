FROM python:3.5.2-slim

COPY ./src/api/ /app/
COPY ./requirements.txt /app/
WORKDIR /app/

RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["app.py"]
